//
//  CGAGradient.h
//  CSDK
//
//  Created by Karoly Gossler on 9/3/12.
//  Copyright (c) 2012 Karoly Gossler. All rights reserved.
//

enum {
	CGLinearGradientDirectionTopBottom,
	CGLinearGradientDirectionBottomTop,
	CGLinearGradientDirectionLeftRight,
	CGLinearGradientDirectionRightLeft,
	CGLinearGradientDirection45Degree,
	CGLinearGradientDirection135Degree,
	CGLinearGradientDirection225Degree,
	CGLinearGradientDirection315Degree
} typedef CGLinearGradientDirection;


void CGContextDrawLinearGradientWithColors(CGContextRef context, CGRect rect, CFArrayRef colors, CGFloat *locations, CGLinearGradientDirection direction);
