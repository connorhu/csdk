//
//  NSAGeometry.h
//  CSDK
//
//  Created by Karoly Gossler on 8/2/12.
//  Copyright (c) 2012 Karoly Gossler. All rights reserved.
//

#import <CoreGraphics/CGGeometry.h>

NS_INLINE CGRect CGRectWithNewWidth(CGRect oldRect, CGFloat width)
{
	oldRect.size.width = width;
    return oldRect;
}

NS_INLINE CGRect CGRectWithNewHeight(CGRect oldRect, CGFloat height)
{
	oldRect.size.height = height;
    return oldRect;
}

NS_INLINE CGRect CGRectWithNewSize(CGRect oldRect, CGSize size)
{
	oldRect.size = size;
    return oldRect;
}

NS_INLINE CGRect CGRectWithNewOrigin(CGRect oldRect, CGPoint origin)
{
	oldRect.origin = origin;
    return oldRect;
}

NS_INLINE CGRect CGRectWithNewOriginX(CGRect oldRect, CGFloat originX)
{
	oldRect.origin.x = originX;
    return oldRect;
}

NS_INLINE CGRect CGRectWithNewOriginY(CGRect oldRect, CGFloat originY)
{
	oldRect.origin.y = originY;
    return oldRect;
}

