//
//  CGAPath.h
//  CSDK
//
//  Created by Karoly Gossler on 9/3/12.
//  Copyright (c) 2012 Karoly Gossler. All rights reserved.
//

enum {
	NSRectCornerTopLeft     = 1 << 0,
	NSRectCornerTopRight    = 1 << 1,
	NSRectCornerBottomLeft  = 1 << 2,
	NSRectCornerBottomRight = 1 << 3,
	NSRectCornerAllCorners  = ~0
};
typedef NSUInteger NSRectCorner;

CGPathRef CGCreateRectanglePathWithCornerRadius(CGRect rect, CGFloat radius, NSRectCorner corner);