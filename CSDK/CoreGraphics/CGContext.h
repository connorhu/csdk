//
//  CGAContext.h
//  CSDK
//
//  Created by Karoly Gossler on 9/3/12.
//  Copyright (c) 2012 Karoly Gossler. All rights reserved.
//

#import "CGGradient.h"

void CGContextFillRectWithLinearGradient(CGContextRef context, CGRect rect, CFArrayRef colors, CGFloat *locations, CGLinearGradientDirection direction);
void CGContextFillRectWithColor(CGContextRef context, CGRect rect, CGColorRef color);
