//
//  CGAContext.m
//  CSDK
//
//  Created by Karoly Gossler on 9/3/12.
//  Copyright (c) 2012 Karoly Gossler. All rights reserved.
//

#import "CGContext.h"

void CGContextFillRectWithLinearGradient(CGContextRef context, CGRect rect, CFArrayRef colors, CGFloat *locations, CGLinearGradientDirection direction)
{
	CGContextSaveGState(context);
	CGContextAddRect(context, rect);
	CGContextClip(context);
	
	CGContextDrawLinearGradientWithColors(context, rect, colors, locations, direction);
	CGContextRestoreGState(context);
}

void CGContextFillRectWithColor(CGContextRef context, CGRect rect, CGColorRef color)
{
	CGContextSetFillColorWithColor(context, color);
	CGContextFillRect(context, rect);
}

