//
//  CGAGradient.m
//  CSDK
//
//  Created by Karoly Gossler on 9/3/12.
//  Copyright (c) 2012 Karoly Gossler. All rights reserved.
//

#import "CGGradient.h"

void CGContextDrawLinearGradientWithColors(CGContextRef context, CGRect rect, CFArrayRef colors, CGFloat *locations, CGLinearGradientDirection direction)
{
#if TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
#else
    CGColorSpaceRef colorSpace = CGColorSpaceCreateWithName(kCGColorSpaceSRGB);
#endif
	
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, colors, locations);
	
	CGPoint startPoint;
	CGPoint endPoint;
	if (direction == CGLinearGradientDirectionTopBottom)
	{
		startPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect));
		endPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect));
	}
	else if (direction == CGLinearGradientDirectionBottomTop)
	{
		startPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect));
		endPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect));
	}
	else if (direction == CGLinearGradientDirection45Degree)
	{
		startPoint = CGPointMake(CGRectGetMinX(rect), CGRectGetMaxY(rect));;
		endPoint = CGPointMake(CGRectGetMaxX(rect), CGRectGetMinY(rect));;
	}
	else if (direction == CGLinearGradientDirection135Degree)
	{
		startPoint = CGPointMake(CGRectGetMaxX(rect), CGRectGetMaxY(rect));;
		endPoint = CGPointMake(CGRectGetMinX(rect), CGRectGetMinY(rect));;
	}
	else if (direction == CGLinearGradientDirection225Degree)
	{
		startPoint = CGPointMake(CGRectGetMaxX(rect), CGRectGetMinY(rect));;
		endPoint = CGPointMake(CGRectGetMinX(rect), CGRectGetMaxY(rect));;
	}
	else if (direction == CGLinearGradientDirection315Degree)
	{
		startPoint = CGPointMake(CGRectGetMinX(rect), CGRectGetMinY(rect));;
		endPoint = CGPointMake(CGRectGetMaxX(rect), CGRectGetMaxY(rect));;
	}
	else if (direction == CGLinearGradientDirectionRightLeft)
	{
		startPoint = CGPointMake(CGRectGetMaxX(rect), CGRectGetMidY(rect));
		endPoint = CGPointMake(CGRectGetMinX(rect), CGRectGetMidY(rect));
	}
	else // CGLinearGradientDirectionLeftRight
	{
		startPoint = CGPointMake(CGRectGetMinX(rect), CGRectGetMidY(rect));
		endPoint = CGPointMake(CGRectGetMaxX(rect), CGRectGetMidY(rect));
	}
	
	CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
	
	CGGradientRelease(gradient);
	CGColorSpaceRelease(colorSpace);
}