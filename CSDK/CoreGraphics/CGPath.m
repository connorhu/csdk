//
//  CGAPath.m
//  CSDK
//
//  Created by Karoly Gossler on 9/3/12.
//  Copyright (c) 2012 Karoly Gossler. All rights reserved.
//

#import "CGPath.h"

CGPathRef CGCreateRectanglePathWithCornerRadius(CGRect rect, CGFloat radius, NSRectCorner corner)
{
	CGMutablePathRef path = CGPathCreateMutable();
	
	// inverz miatt bottom left
	if (corner & NSRectCornerTopLeft)
	{
		CGPathMoveToPoint(path, NULL, rect.origin.x, rect.origin.y + radius);
		CGPathAddCurveToPoint(path, NULL,
							  rect.origin.x, rect.origin.y + radius * .4,
							  rect.origin.x + radius * .4, rect.origin.y,
							  rect.origin.x + radius, rect.origin.y);
	}
	else
		CGPathMoveToPoint(path, NULL,
						  rect.origin.x, rect.origin.y);
	
	// inverz miatt bottom right
	if (corner & NSRectCornerTopRight)
	{
		CGPathAddLineToPoint(path, NULL,
							 CGRectGetMaxX(rect) - radius, rect.origin.y);
		
		CGPathAddCurveToPoint(path, NULL,
							  CGRectGetMaxX(rect) - radius * .4, rect.origin.y,
							  CGRectGetMaxX(rect), rect.origin.y + radius * .4,
							  CGRectGetMaxX(rect), rect.origin.y + radius);
	}
	else
		CGPathAddLineToPoint(path, NULL,
							 CGRectGetMaxX(rect), rect.origin.y);
	
	// top right
	if (corner & NSRectCornerBottomRight)
	{
		CGPathAddLineToPoint(path, NULL,
							 CGRectGetMaxX(rect), CGRectGetMaxY(rect) - radius);
		
		CGPathAddCurveToPoint(path, NULL,
							  CGRectGetMaxX(rect), CGRectGetMaxY(rect) - radius * .4,
							  CGRectGetMaxX(rect) - radius * .4, CGRectGetMaxY(rect),
							  CGRectGetMaxX(rect) - radius, CGRectGetMaxY(rect));
	}
	else
		CGPathAddLineToPoint(path, NULL,
							 CGRectGetMaxX(rect), CGRectGetMaxY(rect));
	
	// top left
	if (corner & NSRectCornerBottomLeft)
	{
		CGPathAddLineToPoint(path, NULL,
							 rect.origin.x + radius, CGRectGetMaxY(rect));
		
		CGPathAddCurveToPoint(path, NULL,
							  rect.origin.x + radius * .4, CGRectGetMaxY(rect),
							  rect.origin.x, CGRectGetMaxY(rect) - radius * .4,
							  rect.origin.x, CGRectGetMaxY(rect) - radius);
	}
	else
		CGPathAddLineToPoint(path, NULL,
							 rect.origin.x, CGRectGetMaxY(rect));
	
	
	CGPathCloseSubpath(path);
	
	CGPathRef newPath = CGPathCreateCopy(path);
	CGPathRelease(path);
	return newPath;
}

