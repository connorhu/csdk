//
//  CServerPooler.m
//  CSDK
//
//  Created by Karoly Gossler on 9/6/12.
//  Copyright (c) 2012 Karoly Gossler. All rights reserved.
//

#import "CServerPooler.h"

#import "CEntryFetcher.h"

@interface CServerPooler ()
{
	NSTimer *poolerTimer;
}

@end

@implementation CServerPooler
@synthesize poolingTimeout = _poolingTimeout;

+ (id) sharedServerPooler
{
    static dispatch_once_t onceQueue;
    static CServerPooler *cServerPooler = nil;
	
    dispatch_once(&onceQueue, ^{ cServerPooler = [[self alloc] init]; });
    return cServerPooler;
}

- (id)init
{
    self = [super init];
    if (self)
	{
		_poolingTimeout = 60 * 10;
    }
    return self;
}

- (void) setPoolingTimeout:(NSTimeInterval) poolingTimeout
{
	_poolingTimeout = poolingTimeout;
	
	if ([self isStarted])
		[self restartPooling];
}

- (BOOL) isStarted
{
	return poolerTimer != nil;
}

- (void) startPooling
{
	if (poolerTimer)
		return;
	
	poolerTimer = [NSTimer scheduledTimerWithTimeInterval:_poolingTimeout
												   target:self selector:@selector(fetch)
												 userInfo:nil
												  repeats:YES];
	[poolerTimer fire];
}

- (void) restartPooling
{
	[self stopPooling];
	[self startPooling];
}

- (void) stopPooling
{
	[poolerTimer invalidate];
	poolerTimer = nil;
}

- (void) fetch
{
	[CEntryFetcher fetch];
}

+ (void) start
{
	[[CServerPooler sharedServerPooler] startPooling];
}

+ (void) stop
{
	[[CServerPooler sharedServerPooler] stopPooling];
}

@end
