//
//  CServerPooler.h
//  CSDK
//
//  Created by Karoly Gossler on 9/6/12.
//  Copyright (c) 2012 Karoly Gossler. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CServerPooler : NSObject
{
	NSTimeInterval _poolingTimeout;
}

@property (nonatomic, assign) NSTimeInterval poolingTimeout;

+ (id) sharedServerPooler;
+ (void) start;
+ (void) stop;

@end
