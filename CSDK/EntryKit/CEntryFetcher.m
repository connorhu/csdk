//
//  CEntryFetcher.m
//  CSDK
//
//  Created by Karoly Gossler on 9/6/12.
//  Copyright (c) 2012 Karoly Gossler. All rights reserved.
//

#import "CEntryFetcher.h"

#import "SBJson.h"

@interface CEntryFetcher ()
{
	NSMutableDictionary *sources;
	NSMutableArray *sourceFetchOrder;
}

@end

@implementation CEntryFetcher

+ (id) sharedEntryFetcher
{
    static dispatch_once_t onceQueue;
    static CEntryFetcher *cEntryFetcher = nil;
	
    dispatch_once(&onceQueue, ^{ cEntryFetcher = [[CEntryFetcher alloc] init]; });
    return cEntryFetcher;
}

- (id) init
{
    self = [super init];
    if (self)
	{
        sources = [[NSMutableDictionary dictionary] retain];
		sourceFetchOrder = [[NSMutableArray array] retain];
    }
    return self;
}

- (void) fetchEntries
{
	for (NSString *key in sourceFetchOrder)
	{
		id<CEntrySource>source = [sources objectForKey:key];
		
		if ([source fetchInProgress])
			continue;
		
		[source fetchWithAfnet];
	}
}

- (void) setSource:(id<CEntrySource>)source withName:(NSString *)name
{
	[sources setObject:source forKey:name];
	[sourceFetchOrder addObject:name];
}

- (id<CEntrySource>) sourceWithName:(NSString *)name
{
	return [sources objectForKey:name];
}

+ (void) setSource:(id<CEntrySource>)source withName:(NSString *)name
{
	[[CEntryFetcher sharedEntryFetcher] setSource:source withName:name];
}

+ (id<CEntrySource>)sourceWithName:(NSString *)name
{
	return [[CEntryFetcher sharedEntryFetcher] sourceWithName:name];
}

+ (void) fetch
{
	[[CEntryFetcher sharedEntryFetcher] fetchEntries];
}

@end
