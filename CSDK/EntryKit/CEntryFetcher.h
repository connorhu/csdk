//
//  CEntryFetcher.h
//  CSDK
//
//  Created by Karoly Gossler on 9/6/12.
//  Copyright (c) 2012 Karoly Gossler. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CEntrySource.h"

@interface CEntryFetcher : NSObject

+ (void) fetch;
+ (void) setSource:(id<CEntrySource>)source withName:(NSString *)name;
+ (id<CEntrySource>)sourceWithName:(NSString *)name;

@end
