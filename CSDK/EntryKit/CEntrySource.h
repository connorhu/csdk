//
//  CEntrySource.h
//  CSDK
//
//  Created by Karoly Gossler on 9/10/12.
//  Copyright (c) 2012 Karoly Gossler. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
	CEntryResultJSONType,
	CEntryResultHTMLType,
	CEntryResultUnknownType
} CEntryResultType;

typedef void (^CSourceResultBlock)(id resultObject, CEntryResultType resultType);

@protocol CEntrySource <NSObject>

- (BOOL) fetchInProgress;

@optional
- (void) fetch:(CSourceResultBlock)resultBlock;
- (void) process:(id)resultObject;
- (void) fetchWithAfnet;

@end
