//
//  NSAGeometry.h
//  CSDK
//
//  Created by Karoly Gossler on 8/2/12.
//  Copyright (c) 2012 Karoly Gossler. All rights reserved.
//

#import <Foundation/NSGeometry.h>

NS_INLINE NSRect NSRectWithNewWidth(NSRect oldRect, CGFloat width)
{
	oldRect.size.width = width;
    return oldRect;
}

NS_INLINE NSRect NSRectWithNewHeight(NSRect oldRect, CGFloat height)
{
	oldRect.size.height = height;
    return oldRect;
}

NS_INLINE NSRect NSRectWithNewSize(NSRect oldRect, NSSize size)
{
	oldRect.size = size;
    return oldRect;
}

NS_INLINE NSRect NSRectWithNewOrigin(NSRect oldRect, NSPoint origin)
{
	oldRect.origin = origin;
    return oldRect;
}

NS_INLINE NSRect NSRectWithNewOriginX(NSRect oldRect, CGFloat originX)
{
	oldRect.origin.x = originX;
    return oldRect;
}

NS_INLINE NSRect NSRectWithNewOriginY(NSRect oldRect, CGFloat originY)
{
	oldRect.origin.y = originY;
    return oldRect;
}

