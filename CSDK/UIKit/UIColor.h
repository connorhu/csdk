//
//  UIAColor.h
//  Connor's iOS Development Kit
//
//  Created by Karoly Gossler on 5/5/12.
//  Copyright (c) 2012 Connor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (hexcolor)

+ (UIColor *) colorWithRGB:(NSUInteger)rgbValue;
+ (UIColor *) colorWithRGB:(NSUInteger)rgbValue 
					 alpha:(CGFloat)alpha;


@end
