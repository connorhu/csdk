//
//  UIAColor.m
//  Connor's iOS Development Kit
//
//  Created by Karoly Gossler on 5/5/12.
//  Copyright (c) 2012 Connor. All rights reserved.
//

#import "UIColor.h"

@implementation UIColor (hexcolor)

+ (UIColor *) colorWithRGB:(NSUInteger)rgbValue
{
	return [self colorWithRGB:rgbValue alpha:1];
}

+ (UIColor *) colorWithRGB:(NSUInteger)rgbValue alpha:(CGFloat)alpha
{
	return [UIColor colorWithRed:((CGFloat)((rgbValue & 0xFF0000) >> 16))/255.0
						   green:((CGFloat)((rgbValue & 0xFF00) >> 8))/255.0 
							blue:((CGFloat)(rgbValue & 0xFF))/255.0 
						   alpha:alpha];
}

@end
