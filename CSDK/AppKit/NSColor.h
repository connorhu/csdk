//
//  NSAGraphicsContext.h
//  CSDK
//
//  Created by Karoly Gossler on 8/2/12.
//  Copyright (c) 2012 Karoly Gossler. All rights reserved.
//

#import <APPKit/NSColor.h>

#if  __MAC_OS_X_VERSION_MIN_REQUIRED == __MAC_10_7

/**
 10_8-tol resze a rendszernek
 */
@interface NSColor (CGColor)

/**
 Return CGColor representation of the NSColor in the RGB color space
 */
@property (readonly) CGColorRef CGColor;

@end

#endif

@interface NSColor (RGB)

// from rgb
+ (NSColor *) colorWithRGB:(NSUInteger)rgbValue;
+ (NSColor *) colorWithRGB:(NSUInteger)rgbValue
					 alpha:(CGFloat)alpha;
+ (NSColor *) colorWithCalibratedRGB:(NSUInteger)rgbValue
							   alpha:(CGFloat)alpha;
+ (NSColor *) colorWithDeviceRGB:(NSUInteger)rgbValue
						   alpha:(CGFloat)alpha;
+ (NSColor *) colorWithSRGB:(NSUInteger)rgbValue;
+ (NSColor *) colorWithSRGB:(NSUInteger)rgbValue
					  alpha:(CGFloat)alpha;

// from string
+ (NSColor *) colorWithRGBString:(NSString *)colorString
						   alpha:(CGFloat)alpha;
+ (NSColor *) colorFromRGBString:(NSString *)colorString;

@end