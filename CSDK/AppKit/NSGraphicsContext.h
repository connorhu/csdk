//
//  NSAGraphicsContext.h
//  CSDK
//
//  Created by Karoly Gossler on 8/2/12.
//  Copyright (c) 2012 Karoly Gossler. All rights reserved.
//

#import <APPKit/NSGraphicsContext.h>

CGContextRef NSGraphicsGetCurrentContext(void);