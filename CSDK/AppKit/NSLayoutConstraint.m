//
//  NSAGraphicsContext.m
//  CSDK
//
//  Created by Karoly Gossler on 8/2/12.
//  Copyright (c) 2012 Karoly Gossler. All rights reserved.
//

#import "NSLayoutConstraint.h"

NSString *NSStringFromEdgeInsets(NSEdgeInsets insets)
{
	return [NSString stringWithFormat:@"{top: %f, bottom: %f, right: %f, left: %f}", insets.top, insets.bottom, insets.right, insets.left];
}