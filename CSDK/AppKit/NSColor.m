//
//  NSAGraphicsContext.m
//  CSDK
//
//  Created by Karoly Gossler on 8/2/12.
//  Copyright (c) 2012 Karoly Gossler. All rights reserved.
//

#import "NSColor.h"

#if  __MAC_OS_X_VERSION_MIN_REQUIRED == __MAC_10_7

/**
 10_8-tol resze a rendszernek
 */
@implementation NSColor (CGColor)

- (CGColorRef)CGColor
{
	NSColor *colorRGB = [self colorUsingColorSpaceName:NSCalibratedRGBColorSpace];
	CGFloat components[4];
	[colorRGB getRed:&components[0] green:&components[1] blue:&components[2] alpha:&components[3]];
	CGColorSpaceRef theColorSpace = CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB);
	CGColorRef theColor = CGColorCreate(theColorSpace, components);
	CGColorSpaceRelease(theColorSpace);
	return theColor;
}

@end

#endif


#pragma mark - NSColor with RGB

@implementation NSColor (RGB)

+ (NSColor *) colorWithSRGB:(NSUInteger)rgbValue
{
	return [NSColor colorWithSRGB:rgbValue alpha:1];
}

+ (NSColor *) colorWithSRGB:(NSUInteger)rgbValue
					  alpha:(CGFloat)alpha
{
	/**
	 *	10_6-ban nincs csak
	 */
	if ([NSColor respondsToSelector:@selector(colorWithSRGBRed:green:blue:alpha:)])
		return [NSColor colorWithSRGBRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0
								   green:((float)((rgbValue & 0xFF00) >> 8))/255.0
									blue:((float)(rgbValue & 0xFF))/255.0
								   alpha:alpha];
	
	CGFloat components[4] = {
		((float)((rgbValue & 0xFF0000) >> 16))/255.0,
		((float)((rgbValue & 0xFF00) >> 8))/255.0,
		((float)(rgbValue & 0xFF))/255.0,
		alpha
	};
	
	return [NSColor colorWithColorSpace:[NSColorSpace sRGBColorSpace]
							 components:components
								  count:4];
}

+ (NSColor *) colorWithRGB:(NSUInteger)rgbValue
{
	return [NSColor colorWithRGB:rgbValue alpha:1];
}

+ (NSColor *) colorWithRGB:(NSUInteger)rgbValue alpha:(CGFloat)alpha
{
	return [NSColor colorWithSRGB:rgbValue alpha:alpha];
}

+ (NSColor *) colorWithCalibratedRGB:(NSUInteger)rgbValue
							   alpha:(CGFloat)alpha
{
	return [NSColor colorWithCalibratedRed:((CGFloat)((rgbValue & 0xFF0000) >> 16))/255.0
									 green:((CGFloat)((rgbValue & 0xFF00) >> 8))/255.0
									  blue:((CGFloat)(rgbValue & 0xFF))/255.0
									 alpha:alpha];
}

+ (NSColor *) colorWithDeviceRGB:(NSUInteger)rgbValue
						   alpha:(CGFloat)alpha
{
	return [NSColor colorWithDeviceRed:((CGFloat)((rgbValue & 0xFF0000) >> 16))/255.0
								 green:((CGFloat)((rgbValue & 0xFF00) >> 8))/255.0
								  blue:((CGFloat)(rgbValue & 0xFF))/255.0
								 alpha:alpha];
}

+ (NSColor *) colorWithRGBString:(NSString *)colorString
						   alpha:(CGFloat)alpha
{
	NSColor *result;
	unsigned int colorCode = 0;
	unsigned char redByte, greenByte, blueByte;
	
	if (nil != colorString)
	{
		NSScanner *scanner = [NSScanner scannerWithString:colorString];
		(void) [scanner scanHexInt:&colorCode];	// ignore error
	}
	redByte		= (unsigned char) (colorCode >> 16);
	greenByte	= (unsigned char) (colorCode >> 8);
	blueByte	= (unsigned char) (colorCode);	// masks off high bits
	result = [NSColor colorWithDeviceRed:(CGFloat)redByte		/ 0xff
								   green:(CGFloat)greenByte	/ 0xff
									blue:(CGFloat)blueByte	/ 0xff
								   alpha:alpha];
	return result;
}

+ (NSColor *) colorFromRGBString:(NSString *)colorString
{
	return [NSColor colorWithRGBString:colorString alpha:1];
}

@end
