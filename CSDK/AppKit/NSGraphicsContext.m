//
//  NSAGraphicsContext.m
//  CSDK
//
//  Created by Karoly Gossler on 8/2/12.
//  Copyright (c) 2012 Karoly Gossler. All rights reserved.
//

#import "NSGraphicsContext.h"

CGContextRef NSGraphicsGetCurrentContext(void)
{
	return [[NSGraphicsContext currentContext] graphicsPort];
}