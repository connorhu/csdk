//
//  CSDK.h
//  CSDK
//
//  Created by Karoly Gossler on 8/2/12.
//  Copyright (c) 2012 Karoly Gossler. All rights reserved.
//


#if (TARGET_OS_MAC && !(TARGET_OS_EMBEDDED || TARGET_OS_IPHONE)) || TARGET_OS_WIN32

// AppKit
#import <CSDK/NSColor.h>
#import <CSDK/NSGraphicsContext.h>
#import <CSDK/NSLayoutConstraint.h>

#import "NSGeometry.h"
#import <CSDK/CGGradient.h>
#import <CSDK/CGPath.h>
#import <CSDK/CGContext.h>
// #import <CSDK/CGAGeometry.h>

#else

#import "UIColor.h"
#import "CGGradient.h"
#import "CGPath.h"
#import "CGContext.h"
#import "CGGeometry.h"

#endif