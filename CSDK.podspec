
Pod::Spec.new do |spec|
  spec.name         = 'CSDK'
  spec.version      = '0.0.1'
  spec.license      = { :type => 'BSD' }
  spec.authors      = { 'Karoly Gossler' => 'connor@connor.hu' }
  spec.summary      = 'AppKit, CoreGraphics, Foundation, UIKit helper functions.'
  spec.social_media_url = 'https://twitter.com/connorhu'
  spec.homepage     = 'http://blog.connor.hu'
  spec.source       = { :git => 'https://bitbucket.org/connorhu/csdk.git' }
  # spec.documentation_url = 'http://blog.connor.hu'
  spec.platform = :osx, "10.7"
  spec.osx.deployment_target = "10.7"
  spec.requires_arc = true
  spec.frameworks = 'Cocoa'
  spec.source_files = "CSDK/AppKit/*.{h,m}", "CSDK/CoreGraphics/*.{h,m}", "CSDK/Foundation/*.{h,m}", "CSDK/CSDK.h"
  spec.ios.source_files = "CSDK/UIKit/*.{h,m}"
end
